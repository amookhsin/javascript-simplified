FROM node:14-alpine

# آرگومان‌ها
ARG USERNAME=node
ARG USER_ID=999
ARG USER_GID=${USER_ID}
ARG WORKDIR=/home/${USERNAME}/app

# نیازمندی‌ها
RUN apk --no-cache add zsh curl git

# ایجادیدن کاربر ناریشه
RUN addgroup -S ${USERNAME} -g ${USER_GID} && \
    adduser -u ${USER_ID} -G ${USERNAME} -D -s /bin/zsh ${USERNAME}
USER ${USERNAME}

ENV NODE_ENV=development \
    NPM_CONFIG_PREFIX=/home/${USERNAME}/.npm-global \
    PATH=$PATH:/home/${USERNAME}/.npm-global/bin

# شخصی‌سازی
RUN sh -c "git clone https://github.com/powerline/fonts.git --depth=1 /tmp/fonts && . /tmp/fonts/install.sh" && \
    sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

# موردنیازها
RUN npm i -g prettier

# پوشه‌کاری
RUN mkdir -p ${WORKDIR}
WORKDIR ${WORKDIR}

# رونشتن پروژه
COPY --chown=${USERNAME}:${USERNAME} ./ ./
